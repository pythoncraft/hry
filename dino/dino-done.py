import sys, pygame
pygame.init()

size = width, height = 900, 200
background = 200, 200, 200
black = (0,0,0)

screen = pygame.display.set_mode(size)
dinos = [pygame.image.load("dino%d.png" % i) for i in range(1, 5)]
kaktus = pygame.image.load("kaktus.png")
ball = dinos[0]

dinorect = dinos[0].get_rect()
kaktusrect = kaktus.get_rect()
kaktusrect.bottom = 170
kaktusrect.left = width + 100
#dinorect = dinorect.move([100, 100])
dinorect.bottom = 169
dinorect.left = 100
print(dinorect.top)
running = False
jumping = False
jumpspeed = [0, 0]
kaktusspeed = [-5, 0]
frame = 0
clock = pygame.time.Clock()
while 1:
    frame += 1
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()
    
    klavesy = pygame.key.get_pressed()
    
    if klavesy[pygame.K_SPACE]:
        jumping = True
        print("jump", jumpspeed)
    
    if jumping:
        if jumpspeed[1] == 0:
            jumpspeed[1] = -5
        elif jumpspeed[1] < 0 and dinorect.top < 20:
            jumpspeed[1] = 5
        elif jumpspeed[1] > 0 and dinorect.bottom > 168:
            jumpspeed[1] = 0
            jumping = False
        dinorect = dinorect.move(jumpspeed)
            
    kaktusrect = kaktusrect.move(kaktusspeed)
  
    screen.fill(background)
    screen.blit(dinos[frame//5%2+1], dinorect)
    screen.blit(kaktus, kaktusrect)
    pygame.draw.line(screen, black, [0, 170], [1000, 170], 2)
    pygame.display.flip()
    clock.tick(40)
